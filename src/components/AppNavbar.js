import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

import { useState, useEffect, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

export default function AppNavbar() {

  //const [ user, setUser ] = useState(localStorage.getItem('email'));

  const { user } = useContext(UserContext);

  console.log(user);

  return (
    <Navbar bg="dark" expand="lg">
      <Navbar.Brand as={Link} to="/" className="mx-5" style={{color: 'white'}}>VELOCITY RIDE</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/" style={{color: 'white'}}>Home</Nav.Link>
          
          {
            (user.id !== null) ?
            <Nav.Link as={NavLink} to="/logout" style={{color: 'white'}}>Logout</Nav.Link>
            :
            <>
            <Nav.Link as={NavLink} to="/login"  style={{color: 'white'}}>Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register" style={{color: 'white'}}>Register</Nav.Link>
            <Nav.Link as={NavLink} to="/admin"   style={{color: 'white'}}>Admin Dashboard</Nav.Link>
           
            </>
          }
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
};