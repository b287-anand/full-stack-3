import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Banner.css'; 

export default function Banner({ data }) {
    const { title, content, destination, label } = data;

    return (
         <div className="home-container " >

        <Row className="banner-container">
            <Col className="banner-content p-5">

                <h1 className="banner-title text-light">{title}</h1>
                <p className="banner-text text-light">{content}</p>

                <Button variant="danger" as={Link} to="/products"  className="banner-button">
                    {label}
                </Button>
            </Col>
        </Row>
        </div>
    );
}
