import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import styles from './ProductView.css';

export default function ProductView() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    // useParams() is a hook that allows us to retrieve productId passed via URL params
    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1); // Default quantity is set to 1

    const enroll = () => {
        fetch('http://localhost:4000/users/checkout', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                productName: name,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data.success) { // Check if the response indicates success
                Swal.fire({
                    title: "Successfully Placed Your Order",
                    icon: "success",
                    text: "You have successfully placed your order."
                })

                navigate("/products");

            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            }
        })
        .catch(error => {
            // Handle fetch errors here
            console.error(error);
        });
    };

    useEffect(() => {
        console.log(productId);

        fetch(`http://localhost:4000/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [productId]);

    return (
        <Container>
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>INR {price}</Card.Text>
                            <Card.Text>Quantity:</Card.Text>
                            <div className={`d-flex justify-content-center align-items-center ${styles['quantity-selector']}`}>
                                <button className={`${styles['quantity-button']}`}
                                    onClick={() => setQuantity(Math.max(1, quantity - 1))}>
                                    -
                                </button>
                                <input
                                    className={`${styles['quantity-input']}`}
                                    type="number"
                                    value={quantity}
                                    onChange={(e) => setQuantity(parseInt(e.target.value))}
                                />
                                <button className={`${styles['quantity-button']}`}
                                    onClick={() => setQuantity(quantity + 1)}>
                                    +
                                </button>
                            </div>
                            <Card.Text>Built Bikes</Card.Text>
                            <Card.Text>In stock</Card.Text>
                            {user.id !== null ? (
                                <Button variant="primary" onClick={enroll}>Add to Cart</Button>
                            ) : (
                                <Button className="btn btn-danger" as={Link} to="/login">Log in to Shop</Button>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
