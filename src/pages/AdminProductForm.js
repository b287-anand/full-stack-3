import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';


import { useNavigate, Navigate } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';



export default function Create() {
  const navigate=useNavigate();




  // State hooks to store the values of the input fields
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [description, setdescription] = useState('')




  async function createProduct(e) {
    e.preventDefault();
    try{

          const product = {
        name: name,
        description: description,
        price: price,
      
      };
               await axios.post(
        `${process.env.REACT_APP_API_URL}/products/`,
        product,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

      


      setName("");
      setdescription("");
      setPrice("");
      
      Swal.fire({

        icon:'success',
        title:'Success!',
        text:'Added Successfully',
        timer:3000,
        timeProgressBar: true,
        showConfirmButton: false,

         

      });
      
      

  } catch (error){
    console.error('Error:', error.response);
    Swal.fire({
      icon:'error',
      title:'Error',
      text:' failed try again',
    });
  }
  }
  

  return(
    
  
    <Form onSubmit={(e) => createProduct(e)}>
    <h1>Add Product</h1>
    
        
    
      <Form.Group controlId="name">
        <Form.Label>Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter name here"
          value={name}
          onChange={e => setName(e.target.value)}
          required
        />
      
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Description"
          value={description}
          onChange={e => setdescription(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label> Price</Form.Label>
        <Form.Control
          type="text"
          placeholder="Price"
          value={price}
          onChange={e => setPrice(e.target.value)}
          required
        />
      </Form.Group>
      


      <Button variant="danger" type="submit" id="submitBtn" >
          Submit
        </Button>

    </Form>
  )
}
