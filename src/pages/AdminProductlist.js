import { useState, useEffect } from 'react';
import {Button} from 'react-bootstrap';

export default function AdminProductList() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    // Fetch products from your API and update the products state
     fetch(`http://localhost:4000/products/all`)
     .then(response => response.json())
     .then(data => setProducts(data));
  }, []);

  const handleRemoveProduct = (productId) => {
 
 setProducts(products.filter(product => product.id !== productId));
  };

  return ( 
    <div>
      <h2>Product List</h2>
            <Button variant="outline-primary">Primary</Button>{' '}
      <ul>
        {products.map(product => (
          <li key={product.id}>
            {product.name} - {product.price}{' '}
            <button onClick={() => handleRemoveProduct(product.id)}>Remove</button>
          </li>
        ))}
      </ul>
      <p> Want to create new product. <a href='/addproduct'>Click here</a></p>
    </div>
  );
}
