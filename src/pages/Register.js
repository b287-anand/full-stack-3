import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate, Navigate } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';



export default function Register() {
	const navigate=useNavigate();

	const { user } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [ email, setEmail ] = useState('');
	const [ firstName, setfname] = useState('');
	const [ lastName, setlname ] = useState('');
	const [ number, setnumber ] = useState('');

	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	// State to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match. 
		if((email !== "" && password1 !== "" && password2 !== "") && (password2 === password1)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	async function registerUser(e) {
		e.preventDefault();
		try{

			    const user = {
        email: email,
        firstName: firstName,
        lastName: lastName,
        number: number,
        password: password1,
      };
      		     await axios.post(
        `${process.env.REACT_APP_API_URL}/users/register`,
        user,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

			


			setEmail("");
			setPassword1("");
			setPassword2("");
			setfname("");
			setlname("");
			setnumber("");
			Swal.fire({

				icon:'success',
				title:'Success!',
				text:'Thank you for registering!',
				timer:3000,
				timeProgressBar: true,
				showConfirmButton: false,

				 

			});
			navigate("/login")
			
		

		

	
			

	} catch (error){
		Swal.fire({
			icon:'error',
			title:'Error',
			text:'Registration failed try again',
		});
	}
	}
	

	return(
		
	
		<Form onSubmit={(e) => registerUser(e)}>
		<h1>REGISTER</h1>
		
				
		
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label> Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			<p align="center">Already have an account.<a href="/login"> click here</a></p>


			{ 
				isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>

			}

		</Form>
	)
}
